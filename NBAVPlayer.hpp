/*
	*
	* NBAVPlayer.hpp - Peek plugin to preview Video and Audio
	*
*/

#pragma once

#include <QtGui>
#include <QtCore>
#include <QtWidgets>

#include <mpv/client.h>

class NBAVPlayer : public QMainWindow {
	Q_OBJECT

	public :
		NBAVPlayer();
		~NBAVPlayer();

	private :
		void createGUI();
		void setWindowProperties();

		QVBoxLayout *avLyt;
		QLabel *lbl;

		QWidget *mpv_container;
		mpv_handle *mpv;

		bool mIsPlaying;

	public Q_SLOTS:
		void show();
		void play( QString path );

		void handleMpvEvents();

	Q_SIGNALS:
		void mpv_events();
};
