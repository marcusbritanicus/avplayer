TEMPLATE = app
TARGET = avplayer

QT += widgets

CONFIG += link_pkgconfig
PKGCONFIG += mpv

HEADERS += NBAVPlayer.hpp
SOURCES += NBAVPlayer.cpp main.cpp

MOC_DIR = build
OBJECTS_DIR = build
UI_DIR = build

CONFIG += silent
