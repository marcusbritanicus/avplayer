/*
	*
	* NBAVPlayer.cpp - Peek plugin to preview Video and Audio
	*
*/

#include "NBAVPlayer.hpp"

static void wakeup( void *ctx ) {

	// This callback is invoked from any mpv thread ( but possibly also
	// recursively from a thread that is calling the mpv API ). Just notify
	// the Qt GUI thread to wake up ( so that it can process events with
	// mpv_wait_event() ), and return as quickly as possible.
	NBAVPlayer *mainwindow = ( NBAVPlayer * )ctx;
	emit mainwindow->mpv_events();
}

NBAVPlayer::NBAVPlayer() : QMainWindow() {

	/* Is video playing */
	mIsPlaying = false;

	/* Create the UI */
	createGUI();
	setWindowProperties();
};

NBAVPlayer::~NBAVPlayer() {

	mpv_terminate_destroy( mpv );
	mpv = NULL;
};

void NBAVPlayer::createGUI() {

	setlocale( LC_NUMERIC, "C" );

	mpv = mpv_create();
	if ( !mpv ) {
		throw std::runtime_error( "failed to create mpv handle" );
	}

	mpv_container = new QWidget( this );
	setCentralWidget( mpv_container );
	mpv_container->setAttribute( Qt::WA_DontCreateNativeAncestors );
	mpv_container->setAttribute( Qt::WA_NativeWindow );

	int64_t wid = mpv_container->winId();
	mpv_set_option( mpv, "wid", MPV_FORMAT_INT64, &wid );

	mpv_set_option_string( mpv, "input-default-bindings", "yes" );
	mpv_set_option_string( mpv, "input-vo-keyboard", "yes" );
	mpv_set_option_string( mpv, "osc", "yes" );

	mpv_observe_property( mpv, 0, "time-pos", MPV_FORMAT_DOUBLE );
	mpv_observe_property( mpv, 0, "track-list", MPV_FORMAT_NODE );
	mpv_observe_property( mpv, 0, "chapter-list", MPV_FORMAT_NODE );

	connect( this, SIGNAL( mpv_events() ), this, SLOT( handleMpvEvents() ), Qt::QueuedConnection );
	mpv_set_wakeup_callback( mpv, wakeup, this );

	if ( mpv_initialize( mpv ) < 0 )
		throw std::runtime_error( "mpv failed to initialize" );
};

void NBAVPlayer::setWindowProperties() {

	setMinimumSize( 720, 540 );

	QDesktopWidget dw;
	int hpos = ( int )( ( dw.width() - 720 ) / 2 );
	int vpos = ( int )( ( dw.height() - 540 ) / 2 );

	setGeometry( hpos, vpos, 720, 540 );
};

void NBAVPlayer::play( QString file ) {

	const char *args[] = { "loadfile", file.toUtf8().data(), NULL };
	mpv_command_async( mpv, 0, args );
};

void NBAVPlayer::show() {

	/* Show the dialog */
	QMainWindow::showMaximized();
};

void NBAVPlayer::handleMpvEvents() {

	while ( mpv ) {
		mpv_event *event = mpv_wait_event( mpv, 0 );
		if ( event->event_id == MPV_EVENT_NONE )
			break;

		switch ( event->event_id ) {
			case MPV_EVENT_SHUTDOWN: {
				mpv_terminate_destroy( mpv );
				mpv = NULL;

				close();

				break;
			}

			case MPV_EVENT_END_FILE: {
				mpv_terminate_destroy( mpv );
				mpv = NULL;

				close();

				break;
			}

			default: {

				break;
			}
		}
	}
};
