#include <QApplication>
#include <QMainWindow>

#include <QDebug>

#include "NBAVPlayer.hpp"

int main ( int argc, char** argv ) {

	QApplication app ( argc, argv );

	NBAVPlayer player;
	if ( argc == 2 ) {

		player.play( argv[ 1 ] );
	}

	else {
		QString fn = QFileDialog::getOpenFileName(
			&player,
			"NBPlayer - Open File",
			QDir::homePath()
		);

		if ( fn.isEmpty() )
			return 256;

		player.play( fn );
	}

	player.show();
	return app.exec();
};
